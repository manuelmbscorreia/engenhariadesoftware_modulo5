# EngenhariaDeSoftware_Modulo5

É necessário executar a classe Payroll Application e entrar com o url "http://localhost:8080/h2-console" no browser.

De seguida é efectuar as seguintes condições:

JDBC URL: jdbc:h2:mem:testdb
User Name: sa
Password: (deixar vazio)

Finalmente, é uma questão de executar código em SQL.

___________________________________________________

Modulo 6

No directorio do projeto correr: "docker build -t nome_da_imagem ."
Depois correr no cmd: "docker run --name your_container_name -p 8080:8080 -d demo_m52550"
Depois aceder a:
http://localhost:8080/employees
ou
http://localhost:8080/orders
